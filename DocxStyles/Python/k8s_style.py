from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.text import WD_UNDERLINE, WD_COLOR_INDEX
from docx.oxml import OxmlElement
from docx.oxml.ns import qn
from docx.shared import RGBColor, Pt

k8s_style = {
    "Normal":
        {
            "font": {
                "size": Pt(10),
                "name": "Helvetica",
            }
        },
    "No Spacing":
        {
            "font": {
                "size": Pt(10),
                "name": "Helvetica",
            }
        },
    "Center":
        {
            "base_style": "Normal",
            "paragraph_format": {
                "alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
            }
        },
    "Disclaimer": {
        "base_style": "Center",
        "shading": "F2F2F2",
        "font": {
            "size": Pt(20)
        },
    },
    "Orange title centered":
        {

            "font": {
                "size": Pt(16)
            },
            "base_style": "Center",

            "font.bold": True,
            "font.color.rgb": RGBColor(0xFF, 0x99, 0x00),
            "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
        },
    "Header": {

        "font.size": Pt(11),
        "base_style": "Normal",
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
        "borders": {
            "top": {}, "left": {}, "right": {}, "bottom": {}
        }
    },

    "Bold white": {

        "font.size": Pt(13),
        "font.color.rgb": RGBColor(0xFF, 0xFF, 0xFF),
        "font.bold": True,
    },

    "OgBgTitle":
        {
            "base_style": "Bold white",
            "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
            "shading": "FF9900"
        },

    "Footer": {
        "font.name": "Helvetica",

        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
        "font.bold": True,

        "font.size": Pt(14),
        "base_style": "Center",

        "font.color.rgb": RGBColor(0xFF, 0x99, 0x00),
    },
    "Table Grid": {

        "font.name": "Helvetica",
    },
    "Heading 2":{

        "font.color.rgb": RGBColor(0xFF, 0x99, 0x00),
    },
    "History table grid": {
        "base_style": "Table Grid",

        "font.size": Pt(10),
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
    },
}
