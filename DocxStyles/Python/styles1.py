styles1 = {
    "Mon style gras italique soulingné centré helvetica grand jaune basé sur normal": {
        "base_style": "Normal",
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
        "font.name": "Helvetica",
        "font.color.rgb": RGBColor(0xFF, 0xFF, 0x00),
        "font.italic": True,
        "font.bold": True,
        "font.underline": True,
        "font.size": Pt(36),
    },
    "Mon autre style basé sur le précédent mais en plus grand avec un soulignement chelou": {
        "font.size": Pt(48),
        "base_style": 0,
        "font.underline": WD_UNDERLINE.DOT_DASH
    },
    "ShortenedYellowName1":
        {
            "base_style": 0,
        },
    "ShortenedYellowName2":
        {
            "base_style": 1,
            "hidden": False
        },
    "zob": {
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
        "font.name": 'Helvetica',
        # "font.highlight_color": WD_COLOR_INDEX.YELLOW,
        "shading": "FF00FF",
        "font.size": Pt(10),
        "font.color.rgb": RGBColor(0xFF, 0xFF, 0xFF)
    },
    "Heading 1": {  # Also edit an existing style !!!
        "font.color.rgb": RGBColor(0xFF, 0xCC, 0x00),

        "shading": "A000FA",
        "font.size": Pt(25),

        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
    },
    "Normal": {  # Also edit an existing style !!!
        "font.name": "Arial",
    },
    "Vulnerability Table Grid": {

        "base_style": "Table Grid",
        "type": WD_STYLE_TYPE.TABLE,
        "font.size": Pt(10),
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
    },
    "My very personal table style": {
        "type": WD_STYLE_TYPE.TABLE,
        "font.size": Pt(10),
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.RIGHT,
    }

}
