i_love_blue = {

    "Heading 1": {
        "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
    },
    "Heading 2": {
        "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
    },
    "Heading 3": {
        "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
    },
    "Heading 4": {
        "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
    },
    "Heading 5": {
        "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
    },
    "Heading 6": {
        "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
    },
    "zob": {
        "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
    },
    "Normal": {
        "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
    },
    "ShortenedYellowName1":
        {

            "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
        },
    "ShortenedYellowName2":
        {

            "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
        },
    "Vulnerability Table Grid":
        {
            "base_style": 'Table Grid',
            "type": WD_STYLE_TYPE.TABLE,
            "font.color.rgb": RGBColor(0x00, 0x00, 0xFF),
        }
}