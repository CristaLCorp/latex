import tempfile

from urllib import request
from urllib.parse import urlparse


def uri_validator(x):
    try:
        result = urlparse(x)
        return all([result.scheme, result.netloc, result.path])
    except:
        return False


def dl_pic_to_tmp(url):
    with tempfile.NamedTemporaryFile(mode="wb") as file:
        request.urlretrieve(url, file.name+".jpg")
        return file.name+".jpg"


class Expando(object):
    pass

class Loader(object):
    def __init__(self):
        self.__dict__['_wrapped_obj'] = Expando()
    def as_dict(self):
        return self.__dict__['_wrapped_obj'].__dict__
    def items(self):
        return self.as_dict().items()
    def __getattr__(self, attr):
        if self.__dict__['_wrapped_obj']:
            if not hasattr(self._wrapped_obj, attr):
                return None

        return getattr(self._wrapped_obj, attr)

    def __setattr__(self, name, value):
        setattr(self._wrapped_obj, name, value)
