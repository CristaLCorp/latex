import os
import re
from os.path import basename, dirname


def process_file(file_path):
    context = {}
    free_md_block = "\n[//]: # (Document: Free Markdown)\n"
    print("Including file", file_path)
    content = free_md_block + open(file_path, mode='r', encoding="utf-8").read()
    def recurse_replace(m):
        return process_file(os.path.join(dirname(file_path), m.group(1)+".md")) + free_md_block

    content = re.sub(r'(?<!!)\[\[(.*?)\]\]', recurse_replace, content)
    return content.strip()
