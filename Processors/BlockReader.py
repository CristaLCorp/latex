import re

from Processors.MiscFuncProcessor import Loader
from Tests.test_regex import test_regex

regex_blocks = r"\[\/\/[^\]]*]\s*:\s*#\s*\(Document\s*:\s*([^)]+)\)"
regex_markdown_arrays_headers = r"(%%[^%]*%%\n+)?(^[^\n]*\|[^\n]*?\|[^\n]*\|?\r?$|^[^\n\|]*\|[^\|\n]+$)\n(^[-\|:\s\t]+\|[-\|:\s\t]+?\|?[-\|:\s\t]*\|?$|^\|[-\|:\s\t]+\|$)"


def cut_blocks(raw_str):
    raw_str = re.sub(
        regex_markdown_arrays_headers,
        lambda m: "\n".join(["[//]: # (Document: Tableau)", m.group(0)]),
        raw_str,
        flags=re.MULTILINE
    )
    cut_array = re.split(regex_blocks, raw_str, flags=re.MULTILINE)[1:]
    it = iter(cut_array)
    my_blocks = [read_block(block_type.strip(), block_content.strip()) for block_type, block_content in zip(it, it)]
    set_vars_in_blocks(my_blocks)
    return my_blocks


def merge_blocks(*blocks):
    merged = Loader()
    for block in blocks:
        for k, v in block.items():
            setattr(merged, k, v)
    return merged


def get_context(blocks):
    return merge_blocks(*[block for block in blocks if block.block_type == "Contexte"])


def set_vars_in_blocks(blocks):
    context = get_context(blocks)
    for block in blocks:
        for k, v in block.items():
            setattr(block, k, re.sub(r"\%([a-z0-9_]+)\%", lambda m: context.as_dict().get(m.group(1), "", ), v,
                                     flags=re.MULTILINE),
                    )


def read_block(block_type, block_content):
    block_vars = Loader()

    regex = r"^»([a-z0-9_]+)\s*:\s*((?:(?:.|\r|\n)(?!^(?:\%\%|»)))*.)(?:\n|\Z)"
    matches = re.finditer(regex, block_content, re.MULTILINE)

    for matchNum, match in enumerate(matches, start=1):
        setattr(block_vars, match.group(1).strip(), match.group(2).strip())
        block_content = block_content.replace(match.group(0), "")
    block_vars.block_type = block_type
    block_pure_text = re.sub(r"^»(\r?\n|\Z)", "", block_content, flags=re.MULTILINE).strip()
    block_pure_text = re.sub(r"^\%\%(\r?\n|\Z)", "", block_pure_text, flags=re.MULTILINE).strip()
    block_vars.block_pure_text = block_pure_text

    return block_vars
