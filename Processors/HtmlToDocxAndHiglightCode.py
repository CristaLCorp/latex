import os
import re
import urllib
from pprint import pprint
import docx
import PIL
from docx.shared import RGBColor, Pt, Cm
from pygments import highlight
from pygments.lexers import guess_lexer
from pygments.formatters.html import HtmlFormatter
from pygments.formatters.img import ImageFormatter

from htmldocx import HtmlToDocx
from htmldocx.h2d import get_filename_from_url, fetch_image, is_url

import markdown
from bs4 import BeautifulSoup
from docx import Document
from docx.enum.text import WD_COLOR


## FIX

class HtmlHighlighterToDocX(HtmlToDocx):
    def __init__(self, local_imgs_root_dir="Notes/Markdown"):
        HtmlToDocx.__init__(self)
        self.local_img_root_dir = local_imgs_root_dir

    def code_to_HTML(self, code, prepare_to_docx=False, noclasses=True):

        html = highlight(code, guess_lexer(code), HtmlFormatter(noclasses=noclasses))
        if prepare_to_docx:
            html = html.replace("\n", "<br>")
        return html

    def pure_makdown_to_html(self, text, auto_number_headings=True, caller_doc=None):
        code_regex = r"```"

        text_split = text.split(code_regex)
        codes = {str(ix): txt for ix, txt in enumerate(text_split) if ix % 2 == 1}
        text_split = [txt if ix % 2 == 0 else f"|||code:{ix}|||" for ix, txt in enumerate(text_split)]
        text = "".join(text_split)

        text = text.replace("\n", "\n\n")

        html_doc = markdown.markdown(text)

        soup = BeautifulSoup(html_doc, 'html.parser')
        # Fixes here
        # Fix first p in lis
        [setattr(p, "name", "span") for p in soup.select("li>p:nth-child(1)")]
        [setattr(img.parent, "name", "span") for img in soup.select("p>img")]

        images_to_size_in_doc = soup.select("img[alt*=img_]")
        for img in images_to_size_in_doc:
            w = re.search("img_width=([0-9]+)", img["alt"])
            h = re.search("img_height=([0-9]+)", img["alt"])
            img["width"] = w.group(1) + "px" if w else "auto"
            img["height"] = h.group(1) + "px" if h else "auto"

        if auto_number_headings:
            for heading in soup.select("h1,h2,h3,h4,h5,h6,h7"):
                heading.string.replace_with(
                    caller_doc.auto_numbered_text(heading.string, int(re.sub('\D', '', heading.name))))
                pass

        html_content = str(soup)
        for ix, code in codes.items():
            if code.strip() == "":
                html_content = html_content.replace(f"|||code:{ix}|||", "")
                continue
            code = "_code_\n" + code
            colored_code = self.code_to_HTML(code, prepare_to_docx=True)
            html_content = html_content.replace(f"|||code:{ix}|||", colored_code)

        return html_content

    def code_to_image(self, code, output_path=None):
        image = highlight(code, guess_lexer(code), ImageFormatter())
        if output_path:
            with open(output_path, "wb") as f:
                f.write(image)
        return image

    def add_styles_to_run(self, style):
        if 'color' in style:
            color = re.sub(r'rgba?|\(|\)', '', style['color'])
            if (color.startswith('#')):
                colors = tuple(int(color.lstrip("#")[i:i + 2], 16) for i in (0, 2, 4))
            else:
                colors = [int(x) for x in color.split(',')]
            self.run.font.color.rgb = RGBColor(*colors)
        if 'background-color' in style:
            color = color = re.sub(r'[a-z()]+', '', style['background-color'])
            colors = [int(x) for x in color.split(',')]
            self.run.font.highlight_color = WD_COLOR.GRAY_25  # TODO: map colors

    def append_html_to_cell(self, html, cell, styled_heading=True, heading_indexed=False):
        if not isinstance(cell, docx.table._Cell):
            raise ValueError('Second argument needs to be a %s' % docx.table._Cell)
        soup = BeautifulSoup(html, "html.parser")
        if styled_heading:
            for heading in soup.select("h1,h2,h3,h4,h5,h6,h7"):
                heading.string.replace_with(f"(heading {heading.name})" + heading.string)

        self.set_initial_attrs(cell)
        self.run_process(str(soup))

        for para in cell.paragraphs:
            if (para.text.startswith("(heading")):
                match = re.match("(\(heading h([0-9])\))", para.text)
                para.style = f"{'' if heading_indexed else '_'}Heading {match.group(2)}"
                para.text = para.text[12:]

    def handle_img(self, current_attrs):

        if not self.include_images:
            self.skip = True
            self.skip_tag = 'img'
            return
        src = current_attrs['src']
        # fetch image
        src_is_url = is_url(src)
        if src_is_url:
            try:
                image = fetch_image(src)
            except urllib.error.URLError:
                image = None
        else:
            image = src if os.path.isabs(src) else os.path.join(self.local_img_root_dir, src)
        # add image to doc
        if image:
            try:
                current_attrs = {key: val for key, val in current_attrs.items() if val not in ("auto", "")}

                h = current_attrs.get("height")
                w = current_attrs.get("width")
                if h:
                    h = Cm(int(re.sub('\D', "", h)))
                if w:
                    w = Cm(int(re.sub('\D', "", w)))
                if not w:
                    from PIL import Image
                    im = Image.open(image)
                    width, height = im.size
                    cm_width = Pt(width)
                    section = self.doc.sections[-1]
                    max_width = section.page_width - (section.left_margin + section.right_margin)
                    if cm_width > max_width:
                        w = max_width

                if isinstance(self.doc, docx.document.Document):
                    self.doc.paragraphs[-1].add_run().add_picture(image, width=w, height=h)
                else:
                    self.doc.paragraphs[-1].add_run().add_picture(image, width=w, height=h)
                if current_attrs.get("alt") and "breaks" in current_attrs.get("alt"):
                    self.doc.paragraphs[-1].runs[-1].add_break()
            except FileNotFoundError:
                image = None
        if not image:
            if src_is_url:
                self.doc.add_paragraph("<image: %s>" % src)
            else:
                # avoid exposing filepaths in document
                self.doc.add_paragraph("<image: %s>" % get_filename_from_url(src))
        # add styles?
