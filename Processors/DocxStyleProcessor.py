import os.path
import re

from docx import Document
from docx.enum.style import WD_STYLE_TYPE
from collections import namedtuple
import docx.enum.text, docx.enum.style
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.text import WD_UNDERLINE, WD_COLOR_INDEX
from docx.oxml import OxmlElement
from docx.oxml.ns import qn
from docx.shared import RGBColor, Pt


import importlib
import json
import sys
import xml
import yaml

def get_style_set_python(styleset_name):
    return getattr(importlib.import_module(f"DocxStyles.Python.{styleset_name}"), styleset_name)

def get_style_set_yaml(styleset_name):
    return yaml.load(open( os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])),"DocxStyles","Yaml", rf"{styleset_name}.yaml"), "r"), Loader=yaml.FullLoader)

def get_style_set_json(styleset_name):
    return json.load(open(rf"DocxStyles/Json/{styleset_name}.json", "r"))

def get_style_set( name, type="python"):
    return getattr(sys.modules[__name__], f"get_style_set_{type}")(name)



def deep_intercept(val, all_styles, document, key=None, parent=None):
    if type(val) is dict:
        for key, val_child in val.items():
            deep_intercept(val_child, all_styles, document, key, val)
    else:
        if type(val) is int and re.search("size|space", key):
            parent[key] = Pt(val)
        if key == "base_style":
            if type(val) is int:
                parent[key] = list(all_styles.keys())[val]
            parent[key] = document.styles[parent[key]]
        for enum in [docx.enum.text, docx.enum.style]:
            if type(val) is str and val.split(".")[0] in enum.__dict__.keys():
                parent[key] = getattr(getattr(enum, val.split(".")[0]), val.split(".")[1])
            if type(val) is str and re.match(r'^[0-9A-Z]{6}$', val):
                parent[key] = RGBColor(*[int(hexcode, 16) for hexcode in re.findall("..", val)])


def deep_set_associative(style, key, value):
    if type(value) is dict:
        [
            deep_set_associative(style.__getattribute__(key), sub_key, sub_value)
            for sub_key, sub_value in value.items()]
    elif key not in ["type"]:
        style.__setattr__(key, value)


def deep_set(style, leset, value):
    if type(leset) is str:
        deep_set(style, leset.split("."), value)
    elif len(leset) > 1:
        deep_set(style.__getattribute__(leset[0]), leset[1:], value)
    elif leset[0] not in ["type"]:
        style.__setattr__(leset[0], value)


def set_style_shading(style, bgcolor):
    bgcolor = str(bgcolor) if type(bgcolor) is RGBColor else bgcolor
    h1xml = style._element
    shd = OxmlElement('w:shd')
    shd.set(qn('w:fill'), bgcolor)
    ppr = h1xml.get_or_add_pPr()
    rpr = h1xml.get_or_add_rPr()
    # ppr.append(shd)or
    h1xml.pPr.append(shd)

    # useless rpr.append(shd) or
    # useless h1xml.rPr.append(shd)


def set_style_border(style, borders):
    h1xml = style._element
    pBdr = OxmlElement('w:pBdr')
    h1xml.get_or_add_pPr()
    for border, bstyle in borders.items():
        border = OxmlElement(f'w:{border}')
        border.set(qn('w:color'), bstyle.get("color", "000000"))
        border.set(qn('w:space'), bstyle.get("color", "2"))
        border.set(qn('w:sz'), bstyle.get("color", "4"))
        border.set(qn('w:val'), bstyle.get("color", "single"))
        pBdr.append(border)
    # ppr.append(shd)or
    h1xml.pPr.append(pBdr)


def get_doc_templated(styles_to_apply, templatename="template_rapport.docx"):
    document = Document(templatename)
    return apply_styleset_on_doc(document, styles_to_apply)


def apply_styleset_on_doc(document, styles_to_apply):
    styles = document.styles

    for style_name, style_details in styles_to_apply.items():
        existing_styles = [x.name for x in [*styles]]
        # print(existing_styles)
        deep_intercept(style_details, styles_to_apply, document)
        style = styles[style_name] if style_name in existing_styles \
            else styles.add_style(style_name, style_details.get("type",
                                                                style_details[
                                                                    "base_style"].type if style_details.get(
                                                                    "base_style") else WD_STYLE_TYPE.PARAGRAPH
                                                                ))
        for property, value in style_details.items():
            if property == "shading":
                set_style_shading(style, value)
            elif property == "borders":
                set_style_border(style, value)
            elif "." in property:
                deep_set(style, property, value)
            else:
                deep_set_associative(style, property, value)
        # print(style)

    existing_styles = [x.name for x in [*styles]]
    for style in document.styles:

        if re.match("Heading [0-9]$", style.name) and  not "_" + style.name in existing_styles:
            not_indexed_heading = document.styles.add_style("_" + style.name,WD_STYLE_TYPE.PARAGRAPH)
            not_indexed_heading.base_style=style
    return document
