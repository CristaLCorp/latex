# coding: utf-8
import sys

from Outputers.Docx.DocxOutputer import output_docx
from Processors import BlockReader
from Processors.ObsidianProcessor import process_file

if __name__ == '__main__':
    # Full featured example !
    # Loading body from MD
    # Applying style from YAML
    args = sys.argv[1:]
    defaults = ["Notes/Markdown/ExempleDocumentComplet/Full featured example.md",
                "Reports",
                "k8s_style",
                "yaml",
                {}
                ]
    args = [
        args[ix] if ix < len(args) else default for ix, default in enumerate(defaults)
    ]
    raw_text = process_file(args[0])
    all_blocks = BlockReader.cut_blocks(raw_text)
    output_docx(*([all_blocks] + args))

    pass
