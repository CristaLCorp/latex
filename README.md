
You want to easily create a shareable document in your compagny from your easy taken notes?

This tool is for you

Current notes tools supported curently
- Obsidian

How to use


You have what i call "Blocks", they always begin with a comment writetn like this [\/\/]: # (Document: Block name)

Each block is like a function call : it has a name (like ... Table of contents) and zero or more parameters:values delimited like this \n»parametername:value
Eg
[\/\/]: # (Table of contents)
»title : Table des matières
[\/\/]: # (Break page)
[\/\/]: # (Calendar)
»january: 31st : Dont forget to call mom
Will simply show the table of contents of your document pre titled with "Table des matières", break a page and show a calendar remindig you the 31st to call your mom.

Often, blocks have default values, so if you are missing one i will probably simply not apprear

You don't have to put all your blocks in one single file as
this program supports Obsidian links [[ file name ...]] to include files in same folder

You can also take notes in markdown in any file right after line starting with »\n any file inclusion or any block start like this

[\/\/]: # (Table of contents)
»title : Table des matières
»
My free markdown here
[\/\/]: # (Break page)
My other free markdown
[\/\/]: # (Calendar)
Free markdown again
[[an included file ]] 
Again, free mardown


To kickstart this program only needs

- Your notes documents sub folder : Demo one is "ExempleDocumentComplet"
- Your notes documennts main file thant includes the others. demo one is "Full featured example"
- The theme used for the document (demo one is k8s_style)
- The theme source for the document (demo one is yaml)
- (Optional) Dict of key val to replace default outputers for blocks (demo one is {})

If no parameters are given thya re assumed

Eg:
  {
  "Table of contents":"My table of contents interpreter"
  }
This will use "My table of contents interpreter" instead of the default one

At any place in the doc you may want to use double percent values %%name%% to reference a value from the context like %%author%%

Each block function signature look like this one
```bash
docx_main_page(context,doc,free_mardown,**kwargs) #Default title is blank
    vars = {{**context},**kwargs} # in this case we want to default values from context if value title is not defined
    add_title(vars["title"])
    add_logo(vars["logo"])
    add_subtitle(vars["title"])
    
or

docx_main_page(context,free_mardown,title="") #Default title is blank
    return "" # table of contents in markdown is impossible to write
```

Context is a dict with key val from context block
context block is the only one tha outputs nothing
if context block is missing context values are assumed

Ouputers are in the BlockInterpreters folder

All block are templated either in The ful featured example or referenced in "Templates" folder for easy inclusion with obsidian templates plugin


Styles are in the Styles folder.
They may be written in python, json or yaml (prefered) format


