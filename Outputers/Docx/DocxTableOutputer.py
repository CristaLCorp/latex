import pprint
import re
from random import random

import numpy
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT

from Outputers.Docx.DocxBaseClass import DocX
from Outputers.Docx.DocxOutputer import functions_references


def matprint(mat):
    for r in mat:
        print(*r)


def get_diagonal_matrix(matrix):
    diagonal_matrix = [[int(e * (ic == round(ir * len(matrix[0]) / len(matrix)))) for ic, e in enumerate(r)] for ir, r
                       in
                       enumerate(matrix
                                 )]
    for ir, row in enumerate(diagonal_matrix):
        if 1 in row:

            if ir < len(diagonal_matrix) - 1 and 1 in diagonal_matrix[ir + 1]:
                for ir2 in range(row.index(1) + 1, diagonal_matrix[ir + 1].index(1)):
                    row[ir2] = 1
            else:
                for ir2 in range(row.index(1) + 1, len(row)):
                    row[ir2] = 1
    return diagonal_matrix


def get_alignment(rows, ic):
    refcelltext = rows[1][ic].strip()
    if refcelltext.startswith(":-") and refcelltext.endswith("-:"):
        return WD_PARAGRAPH_ALIGNMENT.CENTER
    elif refcelltext.endswith("-:"):
        return WD_PARAGRAPH_ALIGNMENT.RIGHT
    return WD_PARAGRAPH_ALIGNMENT.LEFT


def remove_row(table, row):
    tbl = table._tbl
    tr = row._tr
    tbl.remove(tr)


def resolve_func(func_item, row_matrix):
    if callable(func_item):
        func_item = func_item()
    if type(func_item) is list and len(func_item) and type(func_item[0]) is list:
        func_item = [z for i, j in zip(row_matrix, func_item) for z, w in zip(i, j) if w]
    return func_item


def tableau(docx_object: DocX, block, context, blocks):
    splitted_text = re.split(r"(^[^|]*$)", block.block_pure_text.strip(), maxsplit=1, flags=re.MULTILINE)
    block.block_pure_text = "".join(splitted_text[1:])
    rows = [(r_str[0].lstrip("|") + r_str[1:-1] + r_str[-1].rstrip("|")).split("|") for r_str in
            splitted_text[0].strip().splitlines() if r_str.strip() != ""]

    print("Array with headers:", *rows[0])
    table = docx_object.doc.add_table(rows=len(rows), cols=len(rows[0]))
    table.style = block.style or "Table Grid"

    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    for ir, row in enumerate(rows):
        for ic, col in enumerate(row):
            col = col.rstrip()
            if col.replace("%", "") == "»":
                continue
            elif col.replace("%", "") == "↕":
                table.cell(ir, ic).merge(table.cell(ir - 1, ic))
                pass
            else:
                offset = 0
                while len(row) > (ic + 1 + offset) and row[ic + 1 + offset].rstrip("\t").replace("%", "") == "»":
                    offset += 1
                if offset:
                    table.cell(ir, ic).merge(table.cell(ir, ic + offset))
                la_cell = table.cell(ir, ic)
                la_cell.text = col
                for paraph in la_cell.paragraphs:
                    paraph.alignment = get_alignment(rows, ic)
    remove_row(table, table.rows[1])
    if not block.glue_after:
        docx_object.breaks(1)

    row_matrix = [[cell for cell in row.cells] for row in table.rows]

    all_table_cells = [cell for row in table.rows for cell in row.cells]
    even_rows_cells = lambda: [cell for ir, row in enumerate(table.rows) if ir % 2 == 0 for cell in row.cells]
    odd_rows_cells = lambda: [cell for ir, row in enumerate(table.rows) if ir % 2 == 1 for cell in row.cells]
    even_cols_cells = lambda: [cell for ic, columns in enumerate(table.columns) if ic % 2 == 0 for cell in
                               columns.cells]
    odd_cols_cells = lambda: [cell for ic, columns in enumerate(table.columns) if ic % 2 == 1 for cell in
                              columns.cells]

    odd_cells = lambda: [cell for ic, cell in enumerate(all_table_cells) if ic % 2 == 1]
    even_cells = lambda: [cell for ic, cell in enumerate(all_table_cells) if ic % 2 == 0]
    rand_cells = lambda: [cell for cell in all_table_cells if random() >= 0.5]

    matrix = numpy.ones(shape=(len(rows) - 1, len(rows[0])))
    first_mid_rows_cells = lambda: [cell for ir, row in enumerate(table.rows) if ir < len(table.rows) / 2 for
                                    cell in row.cells]
    last_mid_rows_cells = lambda: [cell for ir, row in enumerate(table.rows) if ir >= len(table.rows) / 2 for
                                   cell in row.cells]
    first_mid_columns_cells = lambda: [cell for ic, column in enumerate(table.columns) if
                                       ic < len(table.columns) / 2 for
                                       cell in column.cells]
    last_mid_columns_cells = lambda: [cell for ic, column in enumerate(table.columns) if
                                      ic >= len(table.columns) / 2 for
                                      cell in column.cells]
    top_right_angle_matrix = lambda: [[e * ((ic > ir * len(matrix[0]) / len(matrix))) for ic, e in enumerate(r)] for
                                      ir, r in
                                      enumerate(matrix)]
    bottom_right_angle_matrix = lambda: top_right_angle_matrix()[::-1]
    bottom_left_angle_matrix = lambda: [[e * ((ic < ir * len(matrix[0]) / len(matrix))) for ic, e in enumerate(r)] for
                                        ir, r in
                                        enumerate(matrix
                                                  )]
    top_left_angle_matrix = lambda: bottom_left_angle_matrix()[::-1]
    diagonal_matrix = lambda: get_diagonal_matrix(matrix)
    top_left_corner_matrix = lambda: [
        [int(e * (ic < len(matrix[0]) / 2 and ir < len(matrix) / 2)) for ic, e in enumerate(r)]
        for
        ir, r in
        enumerate(matrix)]
    top_right_corner_matrix = lambda: [r[::-1] for r in top_left_corner_matrix()]
    bottom_left_corner_matrix = lambda: top_left_corner_matrix()[::-1]
    bottom_right_corner_matrix = lambda: top_right_corner_matrix()[::-1]

    funcs = {
        "all_cells": all_table_cells,
        "header_row": table.rows[0].cells,
        "header_column": table.columns[0].cells,
        "diagonal_1": diagonal_matrix,
        "diagonal_2": lambda: diagonal_matrix()[::-1],
        "bottom_left_angle": bottom_left_angle_matrix,
        "bottom_right_angle": bottom_right_angle_matrix,
        "top_right_angle": top_right_angle_matrix,
        "top_left_angle": top_left_angle_matrix,

        "bottom_left_corner": bottom_left_corner_matrix,
        "bottom_right_corner": bottom_right_corner_matrix,
        "top_right_corner": top_right_corner_matrix,
        "top_left_corner": top_left_corner_matrix,
        "even_rows_cells": even_rows_cells,
        "odd_rows_cells": odd_rows_cells,
        "even_cols_cells": even_cols_cells,
        "odd_cols_cells": odd_cols_cells,
        "even_cells": even_cells,
        "odd_cells": odd_cells,
        "rand_cells": rand_cells,
        "first_rows": first_mid_rows_cells,
        "last_rows": last_mid_rows_cells,
        "first_cols": first_mid_columns_cells,
        "last_cols": last_mid_columns_cells,
    }

    for key, color in block.items():
        call_function = docx_object.color_cell if key.endswith("_background") else docx_object.cell_text_color
        clean_key = re.sub(r"_background$|_color$", "", key)
        color = color.lstrip("#")
        if clean_key in funcs.keys():
            resolved = resolve_func(funcs[clean_key], row_matrix)
            for cell in resolved:
                call_function.__call__(cell, color)
        if clean_key.startswith("selected_cell_"):
            x, y = (int(x) for x in clean_key.split("_")[2:4])
            call_function.__call__(table.cell(x, y), color)
        if clean_key.startswith("color_match"):
            x = clean_key.split("_")[2]
            [call_function.__call__(cell, color) for row in table.rows for cell in row.cells if x in cell.text]
        if clean_key.startswith("color_regex"):
            both = color.split(":::")
            color = both[0].strip()
            regex= both[1].strip()
            [call_function.__call__(cell, color) for row in table.rows for cell in row.cells if re.search(regex,cell.text)]
        if clean_key.startswith("selected_row_"):
            x = int(clean_key.split("_")[2])
            [call_function.__call__(cell, color) for cell in table.rows[x].cells]
        if clean_key.startswith("selected_col_"):
            y = int(clean_key.split("_")[2])
            [call_function.__call__(cell, color) for cell in table.columns[y].cells]


functions_references["Tableau"] = tableau
