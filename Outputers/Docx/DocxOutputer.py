import os
import pprint
from os.path import dirname
import re

import dns.resolver as resolver
from docx.enum.text import WD_BREAK

from Outputers.Docx.DocxBaseClass import DocX
from Processors.BlockReader import get_context
from Processors.DocxStyleProcessor import get_style_set
from Processors.HtmlToDocxAndHiglightCode import HtmlHighlighterToDocX

functions_references = {}


def output_docx(blocks, source_notes_file, out_folder, style_name, style_format, overrides):
    source_notes_folder = source_notes_file
    for i in range(255):

        print("Notes source : ", source_notes_folder, source_notes_file)
        source_notes_folder = dirname(source_notes_folder)
        if os.path.isdir(os.path.join(source_notes_folder, ".obsidian")):
            break
    docx_object = DocX(HtmlHighlighterToDocX(source_notes_folder))
    styleset = get_style_set(style_name, style_format)
    docx_object.apply_style(styleset)
    context = get_context(blocks)
    functions_references_override = {**functions_references, **overrides}
    for block in blocks:
        callable_user_func = functions_references_override.get(block.block_type, False)
        should_output_more_text = None
        if callable_user_func:
            print("Ouputing a block:", block.block_type)
            should_output_more_text = callable_user_func(docx_object, block, context, blocks)
        if should_output_more_text in [None, True]:
            docx_object.free_markdown_in_doc(block.block_pure_text, source_notes_folder)

    docx_object.save_document(filename=context.filename, outpufolder=out_folder)


def context_func_one(docx_object: DocX, block, context, blocks):
    used_section = docx_object.doc.sections[-1]
    # margins
    margins_str = block.margins or "1.5, 1.5, 1, 1"
    margins = [float(nn) for nn in margins_str.split(",")]
    docx_object.set_margins(*(margins + [used_section]))

    # Header
    head_parag = used_section.header.paragraphs[0]
    docx_object.runbrk(head_parag, block.title).bold = True
    head_parag.add_run(block.subtitle).bold = True
    head_parag.add_run(f" - {block.date}")
    # Footer
    used_section.footer.paragraphs[0].text = f"{block.compagny_name} - {block.confidentiality}"
    pass


functions_references["Contexte"] = context_func_one


def page_de_garde(docx_object: DocX, block, context, blocks):
    docx_object.breaks(2)
    docx_object.doc.add_paragraph(block.logo, style="Center")
    docx_object.doc.add_paragraph(block.title, "Orange title centered")
    docx_object.breaks(2)
    docx_object.doc.add_paragraph(block.product, "Orange title centered")
    docx_object.doc.add_paragraph(block.audit_environement, "Orange title centered")
    docx_object.breaks(2)
    docx_object.doc.add_paragraph(block.subtitle, "Center")
    docx_object.doc.add_paragraph(block.date, "Center")
    docx_object.doc.add_paragraph(block.authors, "Center")
    docx_object.doc.add_paragraph(block.team, "Center")
    docx_object.doc.add_paragraph(block.associated_team, "Center")

    docx_object.doc.add_paragraph(block.disclaimer, "Disclaimer")
    docx_object.doc.add_page_break()


functions_references["Page de garde"] = page_de_garde


def document_history(docx_object: DocX, block, context, blocks):
    docx_object.doc.add_paragraph(block.title, "OgBgTitle")
    docx_object.add_version_array_to_doc(block)
    docx_object.breaks(1)


functions_references["Historique du document"] = document_history


def table_of_contents(docx_object: DocX, block, context, blocks):
    docx_object.doc.add_paragraph(block.title, "OgBgTitle")
    docx_object.generate_toc()


functions_references["Table des matières"] = table_of_contents


def break_page(docx_object: DocX, block, context, blocks):
    docx_object.breaks(type=WD_BREAK.PAGE)


functions_references["Break page"] = break_page


def dig_a_host(docx_object: DocX, block, context, blocks):
    print("Digging a host", block.host)
    dns_paragraph = docx_object.doc.add_paragraph()
    docx_object.runbrk(dns_paragraph, "Résultat de résolution DNS:").bold = True
    try:
        my_resolver = resolver.Resolver(configure=False)
        my_resolver.nameservers = ["8.8.8.8"]
        answer = my_resolver.resolve(block.host, "A")
        for found in answer.response.answer:
            g1 = re.search("^(.+?)\\. ", str(found)).group(1)
            docx_object.runbrk(dns_paragraph, ("" if " IN A " in g1 else "▼ ") + g1)
        if not " IN A " in str(answer.response.answer[-1]):
            docx_object.runbrk(dns_paragraph, "⚠ Host sans résoltion supplémentaire (Danger)")
        for data in answer:
            docx_object.runbrk(dns_paragraph, str(data))

    except Exception as e:
        docx_object.runbrk(docx_object, "⚠ Échec de résolution DNS")
        pprint.pp(e)


functions_references["Dig"] = dig_a_host
