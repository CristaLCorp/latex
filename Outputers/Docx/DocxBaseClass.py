import os
import re
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.enum.text import WD_BREAK

from docx.shared import Cm, Pt, RGBColor

from docx import Document
from docx.oxml.ns import nsdecls, qn

from docx.oxml import parse_xml, OxmlElement

from Processors.DocxStyleProcessor import apply_styleset_on_doc
from Processors.HtmlToDocxAndHiglightCode import HtmlHighlighterToDocX
from Processors.MiscFuncProcessor import uri_validator, dl_pic_to_tmp


class DocX:
    def __init__(self, html_highlighter=None):
        self.used_title_numbers = {"0": 0}
        self.doc = Document()
        self.has_toc = False
        self.highlighter = html_highlighter or HtmlHighlighterToDocX()

    def apply_style(self, style=None):
        apply_styleset_on_doc(self.doc, style or self.style)

    def before_save(self):
        self.place_images_in_docx()
        self.give_code_style()

    def save_document(self, outpufolder='Reports', filename="Rapport.docx", save_as={"html": 8, "pdf": 17}):
        self.before_save()

        os.makedirs(outpufolder, exist_ok=True)
        definitvepath = os.path.join(outpufolder, filename
                                     )
        print(filename, definitvepath)
        try:
            self.doc.save(
                definitvepath
            )

        except PermissionError as e:
            print(f"Doc save has failed, ensure docx file is not still open")
            return
        # If possible, update TOC automagically
        if os.name == 'nt':

            import pywintypes
            import win32com.client
            word = win32com.client.DispatchEx("Word.Application")
            doc = word.Documents.Open(os.path.abspath(definitvepath))
            try:
                if self.has_toc:
                    doc.TablesOfContents(1).Update()
                if save_as:
                    # https://docs.microsoft.com/fr-fr/office/vba/api/word.wdsaveformat
                    for formatExt, formatNumber in save_as.items():
                        path = os.path.abspath(definitvepath.replace(".docx", f".{formatExt}"))
                        doc.SaveAs(path,
                                   FileFormat=formatNumber)

                        print("Done !", path)
                doc.Close(SaveChanges=True)
            except pywintypes.com_error as e:
                doc.Close(SaveChanges=False)
                print(f"Table of content update & save has failed, ensure docx or pdf file is not still open")

            word.Quit()
        print("Done !", os.path.abspath(definitvepath))

    def set_margins(self, left=1.5, right=1.5, top=1.5, bottom=1.5, section=None):

        sections = self.doc.sections if section == None else [section]
        for section in sections:
            section.top_margin = Cm(top)
            section.bottom_margin = Cm(bottom)
            section.left_margin = Cm(left)
            section.right_margin = Cm(right)
        # Adds a line break in an existing paragraph

    def runbrk(self, paraph, text):
        run = paraph.add_run(text)
        run.add_break()
        return run

    def breaks(self, break_number=1, type=WD_BREAK.LINE):
        paraph = self.doc.add_paragraph().add_run()
        return [paraph.add_break(type) for i in range(0, break_number)]

    def get_all(self, type="runs"):
        rows = [
            row for table in self.doc.tables for row in table.rows
        ]
        cells = [
            cell for row in rows for cell in row.cells
        ]
        paragraphs = [
            paragraph for cell in cells for paragraph in cell.paragraphs
        ]
        if type == "cell_paragraphs":
            return paragraphs

        all_paragraphs = [*paragraphs, *self.doc.paragraphs]
        if type == "paragraphs":
            return all_paragraphs
        runs = [
            run for paragraph in all_paragraphs for run in paragraph.runs
        ]
        return runs

    def give_code_style(self):
        for p in self.get_all("paragraphs"):
            if len(p.runs) and "_code_" in p.text:
                for i, r in enumerate(p.runs):
                    if "_code_" in r.text:
                        p.runs[i].text = r.text.replace("_code_", "", 1).strip()
                        break
                p.style = "Code"
        pass

    def place_images_in_docx(self):
        regex = r"!\[([^\]]*?)\]\(([^)]+)\)"
        regex_split = r"!\[[^\]]*?\]\(([^)]+)\)"
        for paragraph in self.get_all("paragraphs"):
            for run in paragraph.runs:
                if re.search(regex, run.text):
                    matches = re.findall(regex, run.text, re.MULTILINE)
                    run_parts = re.split(regex_split, run.text, re.MULTILINE)

                    run.text = ""
                    for index, part in enumerate(run_parts):
                        if (index % 2) == 0:
                            run.add_text(part)
                            # text treatment
                        else:
                            img_width = re.search("img_width=([0-9]+)", matches[index - 1][0])
                            img_height = re.search("img_height=([0-9]+)", matches[index - 1][0])
                            if img_width:
                                img_width = Cm(int(img_width.group(1)))
                            if img_height:
                                img_height = Cm(int(img_height.group(1)))
                            if uri_validator(part):
                                part = dl_pic_to_tmp(part)
                            must_break = "breaks" in matches[index - 1][0]
                            if must_break:
                                run.add_break()
                            run.add_picture(part, width=img_width, height=img_height)
                            if must_break:
                                run.add_break()
                            # img treatment
                            pass

    @staticmethod
    def replace_markdown_special_images(m, source_note_folder):
        print("File :", m.group(1), source_note_folder)
        for root, dirs, files in os.walk(source_note_folder):
            for file in files:
                if file == m.group(1):
                    full_file_path = os.path.join(root, file)
                    partial_file_path = full_file_path.replace(source_note_folder, "").lstrip("/\\").strip()
                    return f"![{m.group(0)}]({partial_file_path})"
        pass

    def free_markdown_in_doc(self, markdown, source_note_folder):
        if not markdown or markdown.strip() == "":
            return

        markdown = re.sub(r'!\[\[(.*?)\]\]', lambda m: self.replace_markdown_special_images(m, source_note_folder),
                          markdown)
        html_content = self.highlighter.pure_makdown_to_html(markdown, caller_doc=self)
        self.highlighter.add_html_to_document(html_content, self.doc)

    def getHeadingNumber(self, level=1):
        last_max = max(list(self.used_title_numbers.keys())).split(".")
        to_update = [0] * level

        for key, val in enumerate(last_max[:level]):
            to_update[key] = int(val)

        to_update[level - 1] += 1
        key_ret = ".".join([str(u) for u in to_update])
        key = ".".join([str(u).rjust(50, "0") for u in to_update])
        self.used_title_numbers[key] = 0
        return key_ret

    def auto_numbered_text(self, text, level):
        return f"{self.getHeadingNumber(level)} - {text}"

    def auto_numbered_heading(self, text, level):
        self.doc.add_heading(self.auto_numbered_text(text, level), level)

    def add_version_array_to_doc(self, history_block):
        if hasattr(history_block, f'version_{1}'):
            i = 0
            table = self.doc.add_table(1, 4)
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            table.cell(0, 0).text = "Version"
            table.cell(0, 1).text = "Auteur"
            table.cell(0, 2).text = "Date"
            table.cell(0, 3).text = "Commentaire"
            table.cell(0, 0).paragraphs[0].style \
                = table.cell(0, 1).paragraphs[0].style \
                = table.cell(0, 2).paragraphs[0].style \
                = table.cell(0, 3).paragraphs[0].style \
                = "Bold white"
            [self.color_cell(table.cell(0, i, ), "595959") for i in range(0, 4)]
            table.style = 'History table grid'
            while getattr(history_block, f'version_{i + 1}'):
                hist_parts = getattr(history_block, f'version_{i + 1}').split('|')
                row = table.add_row()
                for ix, part in enumerate(hist_parts):
                    row.cells[0].text = str(i + 1)
                    row.cells[ix + 1].text = part

                i = i + 1

    def generate_toc(self):
        # Code for generating Table of Contents
        self.has_toc = True

        document = self.doc
        paragraph = document.add_paragraph()
        run = paragraph.add_run()
        fldChar = OxmlElement('w:fldChar')  # creates a new element
        fldChar.set(qn('w:fldCharType'), 'begin')  # sets attribute on element
        instrText = OxmlElement('w:instrText')
        instrText.set(qn('xml:space'), 'preserve')  # sets attribute on element
        instrText.text = 'TOC \\o "1-3" \\h \\z \\u'  # change 1-3 depending on heading levels you need
        fldChar2 = OxmlElement('w:fldChar')
        fldChar2.set(qn('w:fldCharType'), 'separate')
        fldChar3 = OxmlElement('w:t')
        fldChar3.text = "Right-click to update field."
        fldChar2.append(fldChar3)
        fldChar4 = OxmlElement('w:fldChar')
        fldChar4.set(qn('w:fldCharType'), 'end')
        r_element = run._r
        r_element.append(fldChar)
        r_element.append(instrText)
        r_element.append(fldChar2)
        r_element.append(fldChar4)
        p_element = paragraph._p

    def color_cell(self, cell, bgcolor):
        shading_elm = parse_xml(rf'<w:shd {nsdecls("w")} w:fill="{bgcolor}"/>')
        # shading must create every time
        cell._tc.get_or_add_tcPr().append(shading_elm)

    def cell_text_color(self, cell, color):
        newcolor = RGBColor(*tuple(int(color[i:i + 2], 16) for i in (0, 2, 4)))
        for p in cell.paragraphs:
            for r in p.runs:
                r.font.color.rgb = newcolor

    # Easy edit cell
    def cell(self, cell, text, style, bg_color=None):
        cell.text = text
        cell.paragraphs[0].style = style
        if bg_color:
            self.color_cell(cell, bg_color)

    def from_markdown_to_table_sections(self, table, markdown, title_style="Bold Center",
                                        title_bg="FBD4B4"):
        splitted_md = [x for x in re.split("(^|\n)(# .+)", markdown) if x]
        for content in splitted_md:
            ix = len(table.rows)
            table.add_row()
            table.cell(ix, 0).merge(table.cell(ix, len(table.columns) - 1))
            if content.startswith("# "):
                self.cell(table.cell(ix, 0), content[2:], title_style, title_bg)
            else:
                self.highlighter.append_html_to_cell(
                    self.highlighter.pure_makdown_to_html(content, auto_number_headings=False), table.cell(ix, 0))
